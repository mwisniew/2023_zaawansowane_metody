CC = g++-11
CFLAGS = -std=c++2a -Wall -Wpedantic -fconcepts
LFLAGS = -ldl -lxerces-c -lpthread
NAME = interp
HDRDIR = inc
SRCS = $(wildcard src/*.cpp)
OBJS = $(patsubst src/%.cpp,obj/%.o,$(SRCS))

all: $(OBJS) | plugin
	$(CC) -o $(NAME) $^ $(LFLAGS)

obj/%.o: src/%.cpp | obj/
	$(CC) -c -o $@ $^ -I$(HDRDIR) $(CFLAGS)

obj/:
	mkdir -p $@

plugin: libs/
	cd plugin; make

libs/: 
	mkdir -p $@


.PHONY: build debug clean release memcheck
build: all
build: plugin

debug: clean
debug: CFLAGS += -g -O0 -DDEBUG
debug: build

release: clean
release: CFLAGS += -O2
release: build

clean:
	${RM} $(NAME) obj/*.o

memcheck: clean
memcheck: CFLAGS += -g -O0
memcheck: build
memcheck: 
	valgrind --leak-check=yes ./$(NAME)
