#ifndef LIBINTERFACE_HH
#define LIBINTERFACE_HH

#include <iostream>
#include <dlfcn.h>
#include <cassert>
#include "AbstractInterp4Command.hh"

class LibInterface{
  private:
    void* pLibHnd = nullptr;
    AbstractInterp4Command *(*pCreateCmd)(void) = nullptr;

	public:
  	LibInterface() {}
  	LibInterface(const char* LibName);
  	~LibInterface() { if(pLibHnd != nullptr) dlclose(pLibHnd); }
		
		bool OpenLib(const char* LibName);
		bool Init();
		AbstractInterp4Command* CreateCmd() { return pCreateCmd();}
};

LibInterface::LibInterface(const char* LibName){
  if( OpenLib(LibName) )
    Init();
}

bool LibInterface::OpenLib(const char* LibName){
	return (pLibHnd = dlopen(LibName, RTLD_LAZY)) != nullptr;
}

bool LibInterface::Init(){
	void* pFun = dlsym(pLibHnd,"CreateCmd");
	  if (!pFun) {
		      std::cerr << "!!! Nie znaleziono funkcji CreateCmd" << std::endl;
		          return false;
			    }
	    pCreateCmd = reinterpret_cast<AbstractInterp4Command* (*)(void)>(pFun);
	return true;
}

#endif
