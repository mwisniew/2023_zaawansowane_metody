#ifndef SET4LIBINTERFACES_HH
#define SET4LIBINTERFACES_HH

#include <map>
#include <memory>
#include <string>
#include "LibInterface.hh"

class Set4LibInterfaces : public std::map<std::string, std::shared_ptr<LibInterface>>{
  public:
    Set4LibInterfaces();
    Set4LibInterfaces(const Configuration & config);
};

Set4LibInterfaces::Set4LibInterfaces() {} //: 
  //std::map<std::string, std::shared_ptr<LibInterface>>{{"Move", std::make_shared<LibInterface>("libs/libInterp4Move.so")}, 
  //                                    {"Set", std::make_shared<LibInterface>("libs/libInterp4Set.so")}, 
  //                                    {"Rotate", std::make_shared<LibInterface>("libs/libInterp4Rotate.so")}, 
  //                                    {"Pause", std::make_shared<LibInterface>("libs/libInterp4Pause.so")}} { }
                                      
Set4LibInterfaces::Set4LibInterfaces(const Configuration & config){
  std::vector<std::string> libs_to_load = config.get_names();
  std::string pref("libInterp4");
  std::string suf(".so");
  std::string lib;
  size_t pref_len = pref.size();
  
  for(size_t i = 0; i < libs_to_load.size(); i++){
    size_t name_len = libs_to_load[i].size() - pref_len - suf.size();
    lib = libs_to_load[i].substr(pref_len, name_len);
    this->try_emplace(lib, std::make_shared<LibInterface>((std::string("libs/") + libs_to_load[i]).data()));
  }


}

#endif
