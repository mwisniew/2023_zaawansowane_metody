#ifndef CONFIGURATION_HH
#define CONFIGURATION_HH

#include "MobileObj.hh"

#include <vector>
#include <string>

class Configuration {
  private:
    std::vector<std::string> _plugin_names;
    
    std::vector<MobileObj> _starting_objs;
    std::vector<Vector3D> _shifts;
    std::vector<Vector3D> _scales;
    
    
  public:
    Configuration() {}
    const std::vector<std::string> & get_names() const { return _plugin_names; }
    void add_plugin(const std::string & name) { _plugin_names.push_back(name); }
    
    const std::vector<MobileObj> get_starting_objs() const { return _starting_objs; }
    void add_starting_objs(const MobileObj & obj) { _starting_objs.push_back(obj); }
    
    const std::vector<Vector3D> get_shifts() const { return _shifts; }
    void add_shifts(const Vector3D & shift) { _shifts.push_back(shift); }
    
    const std::vector<Vector3D> get_scales() const { return _scales; }
    void add_scales(const Vector3D & scale) { _scales.push_back(scale); }
    
    ~Configuration() {}
};


#endif
