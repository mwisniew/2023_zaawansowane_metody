#ifndef SCENE_HH
#define SCENE_HH

#include "AbstractScene.hh"
#include "AbstractMobileObj.hh"
#include <map>
#include <memory>
#include <string>

class Scene : public AbstractScene{
  private:
    std::map<std::string, std::shared_ptr<AbstractMobileObj>> _Set_MobileObjs;
  public:
  
    std::shared_ptr<AbstractMobileObj> FindMobileObj(const std::string & name) override;
    void AddMobileObj(std::shared_ptr<AbstractMobileObj> pMobObj) override;
};

std::shared_ptr<AbstractMobileObj> Scene::FindMobileObj(const std::string & name) {
  try{
    std::shared_ptr<AbstractMobileObj> tmp = _Set_MobileObjs.at(name);
    return tmp;
  
  } catch(const std::out_of_range& err){
    return nullptr;
  }
}

void Scene::AddMobileObj(std::shared_ptr<AbstractMobileObj> pMobObj) {
  _Set_MobileObjs.try_emplace(pMobObj->GetName(),pMobObj);
}


#endif
