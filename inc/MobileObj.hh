#ifndef MOBILEOBJ_HH
#define MOBILEOBJ_HH

#include "AbstractMobileObj.hh"
#include "Vector3D.hh"
#include <string>


class MobileObj : public AbstractMobileObj{
    private:
        Vector3D _Trans_m;
        Vector3D _RotAngXYZ_deg;
        std::string name;
    public:
      double GetAng_Roll_deg() const override { return _RotAngXYZ_deg[0]; }
      double GetAng_Pitch_deg() const override { return _RotAngXYZ_deg[1]; }
      double GetAng_Yaw_deg() const override { return _RotAngXYZ_deg[2]; }
      
      void SetAng_Roll_deg(double Ang_Roll_deg) override { _RotAngXYZ_deg[0] = Ang_Roll_deg; }
      void SetAng_Pitch_deg(double Ang_Pitch_deg) override { _RotAngXYZ_deg[1] = Ang_Pitch_deg; }
      void SetAng_Yaw_deg(double Ang_Yaw_deg) override { _RotAngXYZ_deg[2] = Ang_Yaw_deg; }
      
      
      const Vector3D & GetPositoin_m() const override { return _Trans_m; }
      void SetPosition_m(const Vector3D &rPos) override { _Trans_m = rPos; }
      
      void SetName(const char* sName) override { name = sName; }
      const std::string & GetName() const override { return name; }
};


#endif
