#ifndef PROGRAMINTERPRETER_HH
#define PROGRAMINTERPRETER_HH

#include "Scene.hh"
#include "Set4LibInterfaces.hh"
#include "ComChannel.hh"
#include "AbstractInterp4Command.hh"
#include "Vector3D.hh"

#include <string>
#include <vector>

class ProgramInterpreter{
  private:
    Scene _scn;
    Set4LibInterfaces _libs;
    ComChannel _serv;
  public:
    ProgramInterpreter(const Configuration & config);
    
    AbstractInterp4Command* get_command_hnd(const std::string & command_name);
    
    //bool exec_command(AbstractInterp4Command* hnd) { return hnd->ExecCmd(_scn, "", _serv); }
    bool exec_command(std::shared_ptr<AbstractInterp4Command> hnd) { return hnd->ExecCmd(_scn, "", _serv); }

};

ProgramInterpreter::ProgramInterpreter(const Configuration & config) : _libs(config){
  std::vector<MobileObj> conf_objs = config.get_starting_objs();
  std::vector<Vector3D> conf_shifts = config.get_shifts();
  std::vector<Vector3D> conf_scales = config.get_scales();
  
  std::vector<std::string> init_commands;
  init_commands.push_back("Clear \n");
  
  for(MobileObj ob : conf_objs){
    _scn.AddMobileObj(std::shared_ptr<AbstractMobileObj>(new MobileObj(ob)));//std::make_shared<AbstractMobileObj>(ob));
  }
  
  for(size_t i = 0; i < conf_objs.size(); i++){
    std::string command = "AddObj Name=" + conf_objs[i].GetName();
    command += " Shift=(";
    command += std::to_string(conf_shifts[i][0]) + ",";
    command += std::to_string(conf_shifts[i][1]) + ",";
    command += std::to_string(conf_shifts[i][2]) + ")";
    
    command += " Scale=(";
    command += std::to_string(conf_scales[i][0]) + ",";
    command += std::to_string(conf_scales[i][1]) + ",";
    command += std::to_string(conf_scales[i][2]) + ")";
    
    command += " Trans_m=(";
    command += std::to_string(conf_objs[i].GetPositoin_m()[0]) + ",";
    command += std::to_string(conf_objs[i].GetPositoin_m()[1]) + ",";
    command += std::to_string(conf_objs[i].GetPositoin_m()[2]) + ")";
    
    command += " RotXYZ_deg=(";
    command += std::to_string(conf_objs[i].GetAng_Roll_deg()) + ",";
    command += std::to_string(conf_objs[i].GetAng_Pitch_deg()) + ",";
    command += std::to_string(conf_objs[i].GetAng_Yaw_deg()) + ")";
    
    command += " \n";
    
    init_commands.push_back(command);
  }
  
  if(_serv.openConnection()){
    for(std::string cmd : init_commands){
      _serv.send(cmd.c_str());
    }
  } else exit(-1);

}

AbstractInterp4Command* ProgramInterpreter::get_command_hnd(const std::string & command_name){
  try {
    AbstractInterp4Command* commandHnd = _libs.at(command_name)->CreateCmd(); 
    return commandHnd;
    } catch(const std::out_of_range& err){
      return nullptr;    
    }
}
#endif
