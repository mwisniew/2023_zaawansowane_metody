#ifndef COMCHANNEL_HH
#define COMCHANNEL_HH

#include "AbstractComChannel.hh"
#include <mutex>
#include <iomanip>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>


#define PORT  6217

class ComChannel : public AbstractComChannel {
  private:
    int _socket;
    std::mutex guard;
  public:
    void Init(int Socket) override { _socket = Socket; }
    int GetSocket() const override { return _socket; }
    void LockAccess() override { guard.lock(); }
    void UnlockAccess() override { guard.unlock(); }
    std::mutex &UseGuard() override { return guard; }
    void send(const char *sMesg) override;
    bool openConnection();
};

void ComChannel::send(const char *sMesg) {
  ssize_t  IlWyslanych;
  ssize_t  IlDoWyslania = (ssize_t) strlen(sMesg);
  
  #ifdef DEBUG
  std::cerr << sMesg << std::endl;
  #endif
  LockAccess();
  while ((IlWyslanych = write(_socket,sMesg,IlDoWyslania)) > 0) {
    IlDoWyslania -= IlWyslanych;
    sMesg += IlWyslanych;
  }
  if (IlWyslanych < 0) {
    std::cerr << "*** Blad przeslania napisu." << std::endl;
  }
  UnlockAccess();
}

bool ComChannel::openConnection() {
  struct sockaddr_in  DaneAdSerw;

  bzero((char *)&DaneAdSerw,sizeof(DaneAdSerw));

  DaneAdSerw.sin_family = AF_INET;
  DaneAdSerw.sin_addr.s_addr = inet_addr("127.0.0.1");
  DaneAdSerw.sin_port = htons(PORT);


  _socket = socket(AF_INET,SOCK_STREAM,0);

  if (_socket < 0) {
     std::cerr << "*** Blad otwarcia gniazda." << std::endl;
     return false;
  }

  if (connect(_socket,(struct sockaddr*)&DaneAdSerw,sizeof(DaneAdSerw)) < 0)
   {
     std::cerr << "*** Brak mozliwosci polaczenia do portu: " << PORT << std::endl;
     return false;
   }
  return true;
}

#endif
