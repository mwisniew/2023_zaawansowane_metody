#include <iostream>
#include "Interp4Move.hh"


using std::cout;
using std::endl;


extern "C" {
  AbstractInterp4Command* CreateCmd(void);
  const char* GetCmdName() { return "Move"; }
}




/*!
 * \brief
 *
 *
 */
AbstractInterp4Command* CreateCmd(void)
{
  return Interp4Move::CreateCmd();
}


/*!
 *
 */
Interp4Move::Interp4Move(): _v(0.0), _d(0.0), _obj_name("")
{}


/*!
 *
 */
void Interp4Move::PrintCmd() const
{
  /*
   *  Tu trzeba napisać odpowiednio zmodyfikować kod poniżej.
   */
  cout << GetCmdName() << "" << _obj_name << " " << _v << " " << _d << endl;
}


/*!
 *
 */
const char* Interp4Move::GetCmdName() const
{
  return ::GetCmdName();
}


/*!
 *
 */
bool Interp4Move::ExecCmd( AbstractScene      &rScn, 
                           const char         *sMobObjName,
			   AbstractComChannel &rComChann
			 )
{
  std::shared_ptr<AbstractMobileObj> curr_obj = rScn.FindMobileObj(_obj_name);
  if(curr_obj == nullptr){
    #ifdef DEBUG
    std::cerr <<"Brak obiektu " << _obj_name << std::endl;
    #endif
    return false;
  }
  
  double r = curr_obj->GetAng_Roll_deg();
  double p = curr_obj->GetAng_Pitch_deg();
  double y = curr_obj->GetAng_Yaw_deg();
  
  #ifdef DEBUG
    std::cerr << std::endl;
    std::cerr << std::endl;
    std::cerr << _obj_name  << "  "<< r << " " << p << " " << y << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
    #endif
  
  double step = (_v * frame_time) / 1000.0;
  
  for(double dist_left = _d; dist_left > 0; dist_left -= step){
    r = curr_obj->GetAng_Roll_deg();
    p = curr_obj->GetAng_Pitch_deg();
    y = curr_obj->GetAng_Yaw_deg();
  
    Vector3D tmp_vect;
    Vector3D start_pos = curr_obj->GetPositoin_m();
    tmp_vect[0] = step * cos(p * M_PI / 180.0) * cos(y * M_PI / 180.0);
    tmp_vect[1] = step * (cos(r * M_PI / 180.0) * sin(y * M_PI / 180.0) + cos(y * M_PI / 180.0) * sin(p * M_PI / 180.0) * sin(r * M_PI / 180.0));
    tmp_vect[2] = step * (sin(r * M_PI / 180.0) * sin(y * M_PI / 180.0) - cos(r * M_PI / 180.0) * cos(y * M_PI / 180.0) * sin(p * M_PI / 180.0));
    
    
    #ifdef DEBUG
    std::cerr << std::endl;
    std::cerr << std::endl;
    std::cerr << tmp_vect << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
    #endif
    
    curr_obj->SetPosition_m(start_pos + tmp_vect);
  
    std::string server_command = "UpdateObj Name=" + _obj_name;
    server_command += " Trans_m=(";
    server_command += std::to_string(curr_obj->GetPositoin_m()[0]) + ","
                 + std::to_string(curr_obj->GetPositoin_m()[1]) + ","
                 + std::to_string(curr_obj->GetPositoin_m()[2]);
    server_command += ") \n";
  
    #ifdef DEBUG
    std::cerr << server_command;
    #endif
  
    rComChann.send(server_command.c_str());
    std::this_thread::sleep_for(std::chrono::milliseconds(frame_time));
  }
  return true;
}


/*!
 *
 */
bool Interp4Move::ReadParams(std::istream& Strm_CmdsList)
{
  double tmp;
  std::string name;
  
  Strm_CmdsList >> name;
  if(!Strm_CmdsList.good())
    return false;
  _obj_name = name;
  
  Strm_CmdsList >> tmp;
  if(!Strm_CmdsList.good())
    return false;
  _v = tmp;
  
  Strm_CmdsList >> tmp;
  if(!Strm_CmdsList.good())
    return false;
  _d = tmp;
  return true;
}


/*!
 *
 */
AbstractInterp4Command* Interp4Move::CreateCmd()
{
  return new Interp4Move();
}


/*!
 *
 */
void Interp4Move::PrintSyntax() const
{
  cout << "   Move  NazwaObiektu  Szybkosc[m/s]  DlugoscDrogi[m]" << endl;
}
