#include <iostream>
#include "Interp4Rotate.hh"


using std::cout;
using std::endl;


extern "C" {
  AbstractInterp4Command* CreateCmd(void);
  const char* GetCmdName() { return "Rotate"; }
}




/*!
 * \brief
 *
 *
 */
AbstractInterp4Command* CreateCmd(void)
{
  return Interp4Rotate::CreateCmd();
}


/*!
 *
 */
Interp4Rotate::Interp4Rotate(): _ang(0.0), _ax(OX), _ang_v(0.0)
{}


/*!
 *
 */
void Interp4Rotate::PrintCmd() const
{
  /*
   *  Tu trzeba napisać odpowiednio zmodyfikować kod poniżej.
   */
   std::string ax_name;
   
   switch(_ax){
    case OX:
      ax_name = "OX";
      break;
    case OY:
      ax_name = "OY";
      break;
    case OZ:
      ax_name = "OZ";
      break;
   }
  cout << GetCmdName() << " " << ax_name << " " << _ang_v << " " << _ang << endl;
}


/*!
 *
 */
const char* Interp4Rotate::GetCmdName() const
{
  return ::GetCmdName();
}


/*!
 *
 */
bool Interp4Rotate::ExecCmd( AbstractScene      &rScn, 
                           const char         *sMobObjName,
			   AbstractComChannel &rComChann
			 )
{

  std::shared_ptr<AbstractMobileObj> curr_obj = rScn.FindMobileObj(_name);
  if(curr_obj == nullptr){
    return false;
  }
  
  for(double ang_left = _ang; ang_left > 0; ang_left -= (_ang_v * frame_time) / 1000.0){
    
    double old_ang;
    switch(_ax){
      case OX:
        old_ang = curr_obj->GetAng_Roll_deg();
        curr_obj->SetAng_Roll_deg(old_ang + ((_ang_v * frame_time) / 1000.0));
        break;
      case OY:
        old_ang = curr_obj->GetAng_Pitch_deg();
        curr_obj->SetAng_Pitch_deg(old_ang + ((_ang_v * frame_time) / 1000.0));
        break;
      case OZ:
        old_ang = curr_obj->GetAng_Yaw_deg();
        curr_obj->SetAng_Yaw_deg(old_ang + ((_ang_v * frame_time) / 1000.0));
        break;
    }
  
  std::string server_command = "UpdateObj Name=" + _name;
  
  server_command += " RotXYZ_deg=(";
  server_command += std::to_string(curr_obj->GetAng_Roll_deg()) + ","
                 + std::to_string(curr_obj->GetAng_Pitch_deg()) + ","
                 + std::to_string(curr_obj->GetAng_Yaw_deg());
  server_command += ") \n";
  
  #ifdef DEBUG
    std::cerr << server_command;
    #endif
  
  rComChann.send(server_command.c_str());
  std::this_thread::sleep_for(std::chrono::milliseconds(frame_time));
  
  }
  
  return true;
}


/*!
 *
 */
bool Interp4Rotate::ReadParams(std::istream& Strm_CmdsList)
{
  double tmp;
  std::string name;
  std::string ax;
  
  Strm_CmdsList >> name;
  if(!Strm_CmdsList.good())
    return false;
  _name = name;

  Strm_CmdsList >> ax;
  if(!Strm_CmdsList.good())
    return false;
  
  if(ax == "OX"){
    _ax = OX;
  } else if(ax == "OY"){
    _ax = OY;
  } else if(ax == "OZ"){
    _ax = OZ;
  } else {  
    Strm_CmdsList.setstate(std::ios_base::failbit);
    return false;
  }
  
  
  
  Strm_CmdsList >> tmp;
  if(!Strm_CmdsList.good())
    return false;
  _ang_v = tmp;
  
  Strm_CmdsList >> tmp;
  if(!Strm_CmdsList.good())
    return false;
  _ang = tmp;
  
  return true;
}


/*!
 *
 */
AbstractInterp4Command* Interp4Rotate::CreateCmd()
{
  return new Interp4Rotate();
}


/*!
 *
 */
void Interp4Rotate::PrintSyntax() const
{
  cout << "   Rotate  NazwaObiektu  Os PredkoscKatowa [deg/s] Kat [deg]" << endl;
}
