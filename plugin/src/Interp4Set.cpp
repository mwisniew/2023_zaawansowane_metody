#include <iostream>
#include "Interp4Set.hh"


using std::cout;
using std::endl;


extern "C" {
  AbstractInterp4Command* CreateCmd(void);
  const char* GetCmdName() { return "Set"; }
}




/*!
 * \brief
 *
 *
 */
AbstractInterp4Command* CreateCmd(void)
{
  return Interp4Set::CreateCmd();
}


/*!
 *
 */
Interp4Set::Interp4Set(): _x(0.0), _y(0.0), _z(0.0), _ox(0.0), _oy(0.0), _oz(0.0)
{}


/*!
 *
 */
void Interp4Set::PrintCmd() const
{
  /*
   *  Tu trzeba napisać odpowiednio zmodyfikować kod poniżej.
   */
  cout << GetCmdName() << " " << _name << " " << _x << " " << _y << " " << _z << " " << _ox << " " << _oy << " " << _oz << endl;
}


/*!
 *
 */
const char* Interp4Set::GetCmdName() const
{
  return ::GetCmdName();
}


/*!
 *
 */
bool Interp4Set::ExecCmd( AbstractScene      &rScn, 
                           const char         *sMobObjName,
			   AbstractComChannel &rComChann
			 )
{
  std::shared_ptr<AbstractMobileObj> curr_obj = rScn.FindMobileObj(_name);
  if(curr_obj == nullptr){
    return false;
  }
  
  Vector3D tmp_vect;
  tmp_vect[0] = _x;
  tmp_vect[1] = _y;
  tmp_vect[2] = _z;
  curr_obj->SetPosition_m(tmp_vect);
  
  curr_obj->SetAng_Roll_deg(_ox);
  curr_obj->SetAng_Pitch_deg(_oy);
  curr_obj->SetAng_Yaw_deg(_oz);
  
  
  std::string server_command = "UpdateObj Name=" + _name;
  server_command += " Trans_m=(";
  server_command += std::to_string(curr_obj->GetPositoin_m()[0]) + ","
                 + std::to_string(curr_obj->GetPositoin_m()[1]) + ","
                 + std::to_string(curr_obj->GetPositoin_m()[2]);
  server_command += ")";
  
  server_command += " RotXYZ_deg=(";
  server_command += std::to_string(curr_obj->GetAng_Roll_deg()) + ","
                 + std::to_string(curr_obj->GetAng_Pitch_deg()) + ","
                 + std::to_string(curr_obj->GetAng_Yaw_deg());
  server_command += ") \n";
  
  #ifdef DEBUG
  std::cerr << server_command;
  #endif
  
  rComChann.send(server_command.c_str());
  
  return true;
}


/*!
 *
 */
bool Interp4Set::ReadParams(std::istream& Strm_CmdsList)
{
  double tmp;
  std::string name;
  
  Strm_CmdsList >> name;
  if(!Strm_CmdsList.good())
    return false;
  _name = name;
  
  Strm_CmdsList >> tmp;
  if(!Strm_CmdsList.good())
    return false;
  _x = tmp;
  

  Strm_CmdsList >> tmp;
  if(!Strm_CmdsList.good())
    return false;
  _y = tmp;

  
  Strm_CmdsList >> tmp;
  if(!Strm_CmdsList.good())
    return false;
  _z = tmp;

  
  Strm_CmdsList >> tmp;
  if(!Strm_CmdsList.good())
    return false;
  _ox = tmp;
  
  
  Strm_CmdsList >> tmp;
  if(!Strm_CmdsList.good())
    return false;
  _oy = tmp;
  
  
  Strm_CmdsList >> tmp;
  if(!Strm_CmdsList.good())
    return false;
  _oz = tmp;
  
  return true;
}


/*!
 *
 */
AbstractInterp4Command* Interp4Set::CreateCmd()
{
  return new Interp4Set();
}


/*!
 *
 */
void Interp4Set::PrintSyntax() const
{
  cout << "   Set  NazwaObiektu  x y z [m]  kat_OX kat_OY kat_OZ [deg]" << endl;
}
