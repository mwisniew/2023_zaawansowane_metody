#include <iostream>
#include <dlfcn.h>
#include <cstdio>
#include <cassert>
#include <sstream>
#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/util/XMLString.hpp>
#include <thread>
#include <memory>
#include "xmlinterp.hh"
#include <list>
#include "AbstractInterp4Command.hh"
#include "Set4LibInterfaces.hh"
#include "Configuration.hh"
#include "Scene.hh"
#include "ComChannel.hh"
#include "ProgramInterpreter.hh"

using namespace std;
using namespace xercesc;

bool ReadFile(const char* sFileName, Configuration &rConfig)
{
   try {
            XMLPlatformUtils::Initialize();
   }
   catch (const XMLException& toCatch) {
            char* message = XMLString::transcode(toCatch.getMessage());
            cerr << "Error during initialization! :\n";
            cerr << "Exception message is: \n"
                 << message << "\n";
            XMLString::release(&message);
            return 1;
   }

   SAX2XMLReader* pParser = XMLReaderFactory::createXMLReader();

   pParser->setFeature(XMLUni::fgSAX2CoreNameSpaces, true);
   pParser->setFeature(XMLUni::fgSAX2CoreValidation, true);
   pParser->setFeature(XMLUni::fgXercesDynamic, false);
   pParser->setFeature(XMLUni::fgXercesSchema, true);
   pParser->setFeature(XMLUni::fgXercesSchemaFullChecking, true);

   pParser->setFeature(XMLUni::fgXercesValidationErrorAsFatal, true);

   DefaultHandler* pHandler = new XMLInterp4Config(rConfig);
   pParser->setContentHandler(pHandler);
   pParser->setErrorHandler(pHandler);

   try {
     
     if (!pParser->loadGrammar("config/config.xsd",
                              xercesc::Grammar::SchemaGrammarType,true)) {
       cerr << "!!! Plik grammar/actions.xsd, '" << endl
            << "!!! ktory zawiera opis gramatyki, nie moze zostac wczytany."
            << endl;
       return false;
     }
     pParser->setFeature(XMLUni::fgXercesUseCachedGrammarInParse,true);
     pParser->parse(sFileName);
   }
   catch (const XMLException& Exception) {
            char* sMessage = XMLString::transcode(Exception.getMessage());
            cerr << "Informacja o wyjatku: \n"
                 << "   " << sMessage << "\n";
            XMLString::release(&sMessage);
            return false;
   }
   catch (const SAXParseException& Exception) {
            char* sMessage = XMLString::transcode(Exception.getMessage());
            char* sSystemId = xercesc::XMLString::transcode(Exception.getSystemId());

            cerr << "Blad! " << endl
                 << "    Plik:  " << sSystemId << endl
                 << "   Linia: " << Exception.getLineNumber() << endl
                 << " Kolumna: " << Exception.getColumnNumber() << endl
                 << " Informacja: " << sMessage 
                 << endl;

            XMLString::release(&sMessage);
            XMLString::release(&sSystemId);
            return false;
   }
   catch (...) {
            cout << "Zgloszony zostal nieoczekiwany wyjatek!\n" ;
            return false;
   }

   delete pParser;
   delete pHandler;
   return true;
}


bool runPreprocessor(const char* fileName, std::stringstream & sstream){
  std::string preproc("cpp -P ");
  char line[1024];
  
  preproc += fileName;
  
  FILE* pipe = popen(preproc.c_str(), "r");
  if(!pipe){
    std::cerr << "Nie mozna uruchomic preprocesora!" << std::endl;
    return false;
  }
  
  while(fgets(line, 1024, pipe)) //std::cout << line;
    sstream << line;
  
  pclose(pipe);
  return true;
}




int main()
{
  Configuration cfg;
  ReadFile("config/config.xml", cfg);
  
  ProgramInterpreter interp(cfg);
  std::list<std::thread> threads;
  
	std::string fName;
	std::stringstream commandStream;
	
	if(!runPreprocessor("plik", commandStream))
	  return -1;
	
	while(commandStream.good()){
	  commandStream >> fName;
	  if(fName.compare("Begin_Parallel_Actions") == 0){
	    while(commandStream.good() && fName.compare("End_Parallel_Actions")){
	      commandStream >> fName;
	      if(fName.compare("End_Parallel_Actions")){
	        std::shared_ptr<AbstractInterp4Command> commandHnd(interp.get_command_hnd(fName));
	        if(commandHnd != nullptr){
            if(commandHnd->ReadParams(commandStream)){
              std::thread thr(&ProgramInterpreter::exec_command, &interp, commandHnd);
              threads.push_back(std::move(thr));
            }
          } else {
          std::cerr << "Brak obsługi polecenia: " << fName << std::endl;
          commandStream.ignore(std::numeric_limits<streamsize>::max(), commandStream.widen('\n'));
          }
        }
	    
	    }
	    
	    for(std::thread& t : threads){
	      if(t.joinable()){
	        t.join();
	      }
	    }
	  
	  }
	  
    threads.clear();
	}
	


  
}

/*
if(commandStream.good()){
	      AbstractInterp4Command* commandHnd = interp.get_command_hnd(fName); 
        if(commandHnd != nullptr){
          if(commandHnd->ReadParams(commandStream)){
            commandHnd->PrintCmd();
            interp.exec_command(commandHnd);
            delete commandHnd;
            }
        } 
        else{
        std::cerr << "Brak obsługi polecenia: " << fName << std::endl;
        commandStream.ignore(std::numeric_limits<streamsize>::max(), commandStream.widen('\n'));
      }
	  }
*/

